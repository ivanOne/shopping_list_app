import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:shopping_app/shopping_list/pages/shopping_list.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SentryFlutter.init((options) {
      options.dsn = 'https://f5404839c7004d83bbf2ead170ac9f62@o194924.ingest.'
          'sentry.io/5681812';
    },
    appRunner: () => runApp(MyApp()),
  );
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Список покупок',
        navigatorObservers: [
          SentryNavigatorObserver(),
        ],
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: ShoppingListPage(),
      );
  }
}
