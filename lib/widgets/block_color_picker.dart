import 'package:flutter/material.dart';

import '../utils.dart';

// TODO: Так как используется в форме, то стоит наверно уйти в сторону
//  stateless виджета

class BlockPicker extends StatefulWidget {

  BlockPicker({
    @required this.pickerColor,
    @required this.onColorChanged,
  });

  final Color pickerColor;
  final Function onColorChanged;

  @override
  State<StatefulWidget> createState() => _BlockPickerState();
}

class _BlockPickerState extends State<BlockPicker> {
  Color _currentColor;

  @override
  void didUpdateWidget(covariant BlockPicker oldWidget) {
    _currentColor = widget.pickerColor;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    _currentColor = widget.pickerColor;
    super.initState();
  }

  void changeColor(Color color) {
    setState(() => _currentColor = color);
    widget.onColorChanged(color);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 115.0,
      margin: EdgeInsets.only(top: 20),
      child: GridView.count(
        padding: EdgeInsets.symmetric(horizontal: 3),
        primary: false,
        crossAxisCount: 4,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
        children: availableColors.map((Color color) =>
            ColorItem(
              color: color,
              isActiveColor: color.value == _currentColor.value,
              parentChangeColor: changeColor,
            )
        ).toList(),
      ),
    );
  }

}

class ColorItem extends StatelessWidget {
  final Color color;
  final bool isActiveColor;
  final bool withoutShadow;
  final Function parentChangeColor;

  const ColorItem({Key key, this.color, this.isActiveColor,
    this.parentChangeColor, this.withoutShadow: false}) : super(key: key);

  void changeColor() {
    parentChangeColor(color);
  }

  List<BoxShadow> buildBoxShadow() {
    return !withoutShadow ? [BoxShadow(color: color.withOpacity(0.8),
      offset: Offset(1.0, 2.0), blurRadius: 3.0)] : [];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: color,
        boxShadow: buildBoxShadow(),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: changeColor,
          borderRadius: BorderRadius.circular(5.0),
          child: AnimatedOpacity(
            duration: const Duration(milliseconds: 210),
            opacity: isActiveColor ? 1.0 : 0.0,
            child: Icon(
              Icons.done,
              color: useWhiteForeground(color) ? Colors.white : Colors.black,
            ),
          ),
        ),
      ),
    );
  }
}