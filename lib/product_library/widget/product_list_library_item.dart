import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shopping_app/data/sqlite_models.dart';

import 'package:shopping_app/widgets/block_color_picker.dart';
import '../../utils.dart';


class ProductListLibraryItem extends StatelessWidget {
  final Product product;
  final bool isChecked;
  final Function selectProduct;
  final Function deleteProduct;

  const ProductListLibraryItem({
    Key key,
    this.product,
    this.isChecked,
    this.selectProduct,
    this.deleteProduct
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectProduct(product),
      onLongPress: () async {
        var result = await showDropDownMenu(
            context,
            [
              PopupMenuItem<String>(
                value: 'remove',
                child: Text('Удалить'),
              ),
            ]
        );

        if (result != null) {
          deleteProduct(product);
        }
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Row(
          children: <Widget>[
            ColorItem(
              color: product.colorForUI,
              isActiveColor: isChecked,
              parentChangeColor: (color) => selectProduct(product),
              withoutShadow: true,
            ),
            SizedBox(width: 10),
            Expanded(child: Text(
                product.name,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    textBaseline: TextBaseline.ideographic
                )
            )),
          ],
        ),
      ),
    );
  }
}