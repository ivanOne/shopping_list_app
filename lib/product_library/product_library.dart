import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shopping_app/data/sqlite_models.dart';

import 'package:shopping_app/data/sqlite_provider.dart';
import 'package:shopping_app/product_library/widget/product_list_library_item.dart';


class ProductLibraryList extends StatefulWidget {
  final List<int> selectedProducts;
  final int slKey;

  const ProductLibraryList({
    Key key,
    this.selectedProducts,
    this.slKey,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => ProductLibraryListState();

}

class ProductLibraryListState extends State<ProductLibraryList> {
  List<int> selectedProducts;
  Future<List<Product>> _loadProducts;

  @override
  void initState() {
    _loadProducts = DBProvider.db.getAllProducts();
    selectedProducts = widget.selectedProducts;
    super.initState();
  }

  void loadProducts() {
    setState(() {
      _loadProducts = DBProvider.db.getAllProducts();
    });
  }

  void selectProduct(Product product) async {
    if(selectedProducts.contains(product.id)) {
      await DBProvider.db.deleteShoppingListProductByProductId(
          widget.slKey,
          product.id
      );

      setState(() {
        selectedProducts.removeWhere((id) => id == product.id);
      });
    }
    else {
      DBProvider.db.createShoppingListProduct(
          widget.slKey,
          product.name,
          null,
          product.color,
      );
      setState(() {
        selectedProducts.add(product.id);
      });
    }
  }

  void deleteProduct(Product product) async {
    await DBProvider.db.deleteProduct(product.id);
    loadProducts();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Все товары')),
      body: Container(
        child: FutureBuilder<List<Product>>(
          future: _loadProducts,
          builder: (BuildContext context, AsyncSnapshot<List<Product>> snapshot)
          {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data.length,
                padding: EdgeInsets.all(8),
                itemBuilder: (context, index) {
                  return ProductListLibraryItem(
                    product:  snapshot.data[index],
                    isChecked: widget.selectedProducts.contains(
                        snapshot.data[index].id
                    ),
                    selectProduct: selectProduct,
                    deleteProduct: deleteProduct,
                  );
                },
              );
            } else if (snapshot.hasError) {
              return Center(
                  child: Text(
                      'Произошла ошибка',
                      style: TextStyle(fontSize: 20))
              );
            } else {
              return Center(
                  child: Text(
                      'Загрузка',
                      style: TextStyle(fontSize: 20))
              );
            }

          },
        )
      ),
    );
  }

}