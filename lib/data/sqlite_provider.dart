import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shopping_app/data/sqlite_models.dart';
import 'package:sqflite/sqflite.dart';


class DBProvider {
  final String shoppingListTable = 'ShoppingList';
  final String productTable = 'Product';
  final String shoppingListProductTable = 'ShoppingListProduct';

  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "ShoppingListDB.db");
    return await openDatabase(
        path,
        version: 1,
        onCreate: (Database db, int version) async {
          await db.execute("CREATE TABLE $shoppingListTable ("
              "id INTEGER PRIMARY KEY, "
              "color INTEGER NOT NULL, "
              "name TEXT NOT NULL"
              ")");
          // TODO: Добавить уникальность названию продукта
          await db.execute("CREATE TABLE $productTable ("
              "id INTEGER PRIMARY KEY, "
              "name TEXT NOT NULL UNIQUE, "
              "color INTEGER NOT NULL"
              ")");
          await db.execute("CREATE TABLE $shoppingListProductTable ("
              "id INTEGER PRIMARY KEY, "
              "quantity TEXT, "
              "checked INTEGER NOT NULL, "
              "productId INTEGER NOT NULL, "
              "shoppingListId INTEGER NOT NULL, "
              "FOREIGN KEY(productId) REFERENCES Product(id)"
              " ON DELETE CASCADE, "
              "FOREIGN KEY(shoppingListId) REFERENCES ShoppingList(id) "
              " ON DELETE CASCADE"
              ")");
        });
  }

  Future<void> createShoppingList(ShoppingList sl) async {
    final db = await database;
    Map<String, dynamic> slMap = sl.toMap();
    slMap.remove('id');
    await db.insert(shoppingListTable, slMap);
  }

  Future<List<ShoppingList>> allShoppingList() async {
    final db = await database;
    List<ShoppingList> result = [];

    for (Map<String, dynamic> item in await db.query(shoppingListTable)) {
      result.add(ShoppingList.fromMap(item));
    }
    return result;
  }

  Future<void> deleteShoppingList(int id) async {
    final db = await database;
    await db.delete(shoppingListProductTable, where: 'shoppingListId = ?',
        whereArgs: [id]);
    await db.delete(shoppingListTable, where: 'id = ?', whereArgs: [id]);
  }

  Future<void> deleteAllShoppingLists() async {
    final db = await database;
    await db.delete(shoppingListTable);
  }

  Future<void> updateShoppingList(ShoppingList updatedShoppingList) async {
    final db = await database;

    Map<String, dynamic> updateValuesMap = updatedShoppingList.toMap();
    int id = updateValuesMap['id'];
    updateValuesMap.remove('id');

    await db.update(shoppingListTable, updateValuesMap, where: 'id = ?',
        whereArgs: [id]);
  }

  Future<void> createShoppingListProduct(
      int shoppingListId,
      String name,
      String quantity,
      int color
      ) async {
    final db = await database;

    int productId = await _updateOrCreateProduct(db, name, color);
    Map<String, dynamic> createValuesMap = {
      'quantity': quantity,
      'productId': productId,
      'shoppingListId': shoppingListId,
      'checked': 0,
    };
    await db.insert(
        shoppingListProductTable,
        createValuesMap
    );
  }

  Future<List<ShoppingListProduct>> shoppingListProducts(int shoppingListId)
  async {
    String query = 'SELECT $shoppingListProductTable.id, '
        '$shoppingListProductTable.productId, $productTable.name as name, '
        '$productTable.color as color, '
        'quantity, checked '
        'FROM $shoppingListProductTable '
        'INNER JOIN $productTable '
        'ON $shoppingListProductTable.productId = $productTable.id '
        'WHERE $shoppingListProductTable.shoppingListId = ? '
        'ORDER BY checked, color, name ASC;';
    List<ShoppingListProduct> result = [];

    final db = await database;
    for (Map<String, dynamic> item in await db.rawQuery(
        query, [shoppingListId]
    )) {
      result.add(ShoppingListProduct.fromMap(item));
    }

    return result;
  }

  Future<void> updateShoppingListProduct(int id, String name, String quantity,
      int color) async {
    final db = await database;

    int productId = await _updateOrCreateProduct(db, name, color);
    Map<String, dynamic> updateValuesMap = {
      'quantity': quantity,
      'productId': productId
    };

    await db.update(
        shoppingListProductTable,
        updateValuesMap,
        where: 'id = ?',
        whereArgs: [id]
    );
  }

  Future<void> deleteShoppingListProduct(int id) async {
    final db = await database;

    await db.delete(shoppingListProductTable, where: 'id = ?', whereArgs: [id]);
  }

  Future<void> deleteShoppingListProductByProductId(int slId, int productId)
  async {
    final db = await database;

    await db.delete(
        shoppingListProductTable,
        where: 'productId = ? and shoppingListId = ?',
        whereArgs: [productId, slId]
    );
  }

  Future<void> clearShoppingList(int id, {String select: 'all'}) async {
    final db = await database;
    String where = 'shoppingListId = ?';

    switch (select) {
      case 'all': {}
      break;

      case 'checked': { where += ' AND checked = 1'; }
      break;

      case 'unchecked': { where += ' AND checked = 0'; }
      break;
    }
    await db.delete(shoppingListProductTable, where: where, whereArgs: [id]);
  }

  Future<void> checkShoppingListProduct(int id) async {
    final db = await database;

    List<Map<String, dynamic>> shoppingListProduct = await db.query(
        shoppingListProductTable, 
        where: 'id = ?', 
        whereArgs: [id]
    );

    if (shoppingListProduct.isNotEmpty) {
      Map<String, int> updData = {
        'checked': shoppingListProduct[0]['checked'] == 0 ? 1 : 0
      };

      await db.update(
          shoppingListProductTable,
          updData, where: 'id = ?',
          whereArgs: [id]
      );
    }
  }

  Future<void> setAllProductInShoppingList(
      int idShoppingList,
      {bool check: true}
      ) async {

    final db = await database;

    await db.update(
        shoppingListProductTable,
        {'checked': check ? 1 : 0 },
        where: 'shoppingListId = ?',
        whereArgs: [idShoppingList]
    );
  }

  Future<int> _updateOrCreateProduct(Database db, String name, int color)
  async {
    List<Map<String, dynamic>> products = await db.query(
        productTable,
        where: 'name = ?',
        whereArgs: [name]
    );

    if (products.isEmpty) {
      return await db.insert(productTable, {'name': name, 'color': color});
    } else {
      Map<String, dynamic> product = Map.of(products[0]);
      int id = product['id'];

      product.remove('id');
      product['name'] = name;
      product['color'] = color;
      await db.update(
          productTable,
          product,
          where: 'id = ?',
          whereArgs: [id]
      );
      return id;
    }
  }

  Future<List<String>> searchProducts(String pattern) async {
    final db = await database;

    List<Map<String, dynamic>> products = await db.query(
        productTable,
        where: 'name LIKE ?',
        whereArgs: ['%$pattern%'],
        columns: ['name']
    );

    List<String> result = [];
    if (products.isNotEmpty) {
      products.forEach((element) => result.add(element['name']));
    }

    return result;
  }

  Future<Product> getProductByName(String name) async {
    final db = await database;

    List<Map<String, dynamic>> products = await db.query(
        productTable,
        where: 'name = ?',
        whereArgs: [name]
    );

    Product result;
    if (products.isNotEmpty || products.length == 1) {
      result = Product.fromMap(products[0]);
    }
    return result;
  }

  Future<List<Product>> getAllProducts() async {
    final db = await database;

    List<Map<String, dynamic>> products = await db.query(
        productTable,
        orderBy: 'name ASC'
    );

    List<Product> result = [];
    if (products.isNotEmpty) {
      products.forEach((element) => result.add(Product.fromMap(element)));
    }

    return result;
  }

  Future<void> deleteProduct(int productId) async {
    final db = await database;

    await db.delete(productTable, where: 'id = ?', whereArgs: [productId]);
  }
}