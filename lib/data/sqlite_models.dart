
import 'package:flutter/material.dart';

abstract class DbModel {
  factory DbModel.fromMap(Map<String, dynamic> map) => null;
  toMap() => throw UnimplementedError();
}

class ShoppingList implements DbModel {
  int id;
  String name;
  num color;

  ShoppingList(this.id, this.name, this.color);

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'color': color,
    };
  }

  Color get colorForUI => Color(color);

  @override
  factory ShoppingList.fromMap(Map<String, dynamic> data) =>
      ShoppingList(data['id'], data['name'], data['color']);

}


class Product implements DbModel {
  int id;
  String name;
  num color;

  Product(this.id, this.name, this.color);

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'color': color,
    };
  }

  Color get colorForUI => Color(color);

  @override
  factory Product.fromMap(Map<String, dynamic> data) =>
      Product(data['id'], data['name'], data['color']);

}


class ShoppingListProduct implements DbModel {
  int id;
  int productId;
  String name;
  num color;
  String quantity;
  bool check;

  ShoppingListProduct(
      this.id,
      this.productId,
      this.name,
      this.color,
      this.quantity,
      this.check,
      );

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'productId': this.productId,
      'name': name,
      'color': color,
      'quantity': quantity,
      'check': check
    };
  }

  Color get colorForUI => Color(color);

  String get nameWithQuantity {
    if (quantity != null) {
      return '$name: $quantity';
    }
    return '$name';
  }

  Color get colorWithCheckMark {
    if (check) {
      return colorForUI.withOpacity(0.2);
    }
    return colorForUI;
  }

  @override
  factory ShoppingListProduct.fromMap(Map<String, dynamic> data) =>
      ShoppingListProduct(
          data['id'],
          data['productId'],
          data['name'],
          data['color'],
          data['quantity'],
          data['checked'] != 0,
      );

}