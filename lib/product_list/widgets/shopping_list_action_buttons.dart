import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_app/data/sqlite_models.dart';
import 'package:shopping_app/product_library/product_library.dart';
import 'package:shopping_app/product_list/bloc/product_list_bloc.dart';

import 'form_modal.dart';


class ShoppingListAddActions extends StatelessWidget {
  final Color colorShoppingList;
  final int slKey;

  const ShoppingListAddActions({
    Key key,
    this.colorShoppingList,
    this.slKey,
  })
      : super(key: key);

  List<int> slProductsToProductKeyList(ProductListState state) {
    List<int> checkedKeyList = [];
    if (state is ProductListLoaded && state.viewModel.productLists.isNotEmpty) {
      state.viewModel.productLists.forEach((ShoppingListProduct slProduct) =>
          checkedKeyList.add(slProduct.productId));
    }

    return checkedKeyList;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        FloatingActionButton(
          heroTag: 'add_from_library',
          onPressed: () async {
            List<int> checkedSlProducts = slProductsToProductKeyList(
                context.read<ProductListBloc>().state
            );
            await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProductLibraryList(
                      slKey: slKey,
                      selectedProducts: checkedSlProducts,
                    )
                )
            );
            context.read<ProductListBloc>().add(ProductListLoad());
          },
          materialTapTargetSize: MaterialTapTargetSize.padded,
          child: const Icon(Icons.book),
          backgroundColor: colorShoppingList,
        ),
        SizedBox(
          height: 16.0,
        ),
        FloatingActionButton(
            child: Icon(Icons.add),
            heroTag: 'add_new',
            backgroundColor: colorShoppingList,
            onPressed: () async {
              var data = await showProductModalForm(
                  context: context,
                  successLabelButton: 'Добавить',
                  nameLabel: 'Название',
                  title: 'Добавить новый продукт',
              );
              if (data != null && data.isNotEmpty) {
                context.read<ProductListBloc>().add(ProductCreate(
                    data['name'],
                    data['color'],
                    data['quantity']
                ));
              }
            }
        )
      ],
    );
  }

}