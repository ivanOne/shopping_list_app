import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_app/data/sqlite_models.dart';
import 'package:shopping_app/product_list/bloc/product_list_bloc.dart';

import '../../utils.dart';
import 'form_modal.dart';



class ProductListItem extends StatelessWidget {
  final ShoppingListProduct slProduct;

  const ProductListItem({Key key, this.slProduct})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        context.read<ProductListBloc>().add(ProductCheck(slProduct.id));
      },
      onLongPress: () async {
        var result = await showDropDownMenu(
            context,
            [
              PopupMenuItem<String>(
                value: 'remove',
                child: Text('Удалить'),
              ),
              PopupMenuItem<String>(
                value: 'edit',
                child: Text('Редактировать'),
              ),
            ]
        );
        if (result != null) {
          switch (result) {
            case 'remove':
              context.read<ProductListBloc>().add(ProductDelete(slProduct.id));
              break;
            case 'edit':
              var data = await showProductModalForm(
                  context: context,
                  initialName: slProduct.name,
                  initialColor: slProduct.colorForUI,
                  initialQuantity: slProduct.quantity,
                  successLabelButton: 'Изменить',
                  nameLabel: 'Название',
                  title: 'Изменить ${slProduct.name}',
              );
              if (data != null && data.isNotEmpty) {
                context.read<ProductListBloc>().add(ProductUpdate(
                    slProduct.id,
                    data['name'],
                    data['color'],
                    data['quantity'],
                ));
              }
          }
        }
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Row(
          children: <Widget>[
            Container(
                height: 30.0,
                width: 5.0,
                color: slProduct.colorWithCheckMark
            ),
            SizedBox(width: 10),
            Expanded(child: Text(
              slProduct.nameWithQuantity,
              style: TextStyle(
                  color: slProduct.check ? Colors.black.withOpacity(0.1) :
                    Colors.black,
                  fontSize: 20,
                  textBaseline: TextBaseline.ideographic
              )
            )),
          ],
        ),
      ),
    );
  }
}