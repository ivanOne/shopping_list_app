import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:shopping_app/data/sqlite_models.dart';
import 'package:shopping_app/data/sqlite_provider.dart';
import 'package:shopping_app/widgets/block_color_picker.dart';

import '../../utils.dart';



Future<Map<String, dynamic>> showProductModalForm({
  @required BuildContext context,
  @required String title,
  @required String nameLabel,
  @required String successLabelButton,
  String initialName,
  Color initialColor,
  String initialQuantity
}){
  return showDialog<Map<String, dynamic>>(
    context: context,
    builder: (BuildContext context) {
      return ProductDialog(
        initialName: initialName,
        initialColor: initialColor,
        initialQuantity: initialQuantity,
        title: title,
        nameLabel: nameLabel,
        successLabelButton: successLabelButton,
      );
    },
  );
}

class ProductDialog extends StatefulWidget {
  final String initialName;
  final Color initialColor;
  final String initialQuantity;
  final String title;
  final String nameLabel;
  final String successLabelButton;

  const ProductDialog({
    Key key,
    this.initialName,
    this.initialColor,
    this.initialQuantity,
    this.title,
    this.nameLabel,
    this.successLabelButton,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => ProductDialogState();

}

class ProductDialogState extends State<ProductDialog> {
  Color currentColor;
  Color pickedColor;
  TextEditingController nameController;
  TextEditingController quantityController;
  bool nameIsValid;

  @override
  void initState() {
    nameIsValid = true;
    nameController = TextEditingController();
    quantityController = TextEditingController();

    if (this.widget.initialColor != null) {
      currentColor = this.widget.initialColor;
    } else {
      currentColor = availableColors[0];
    }

    if (this.widget.initialName != null) {
      nameController.text = this.widget.initialName;
    }
    if (this.widget.initialQuantity != null) {
      quantityController.text = this.widget.initialQuantity;
    }
    super.initState();
  }

  void changeColor(Color color) {
    setState(() {
      currentColor = color;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(this.widget.title),
      content: SingleChildScrollView(
        child: Container(
          width: 200,
          height: 230,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TypeAheadFormField(
                textFieldConfiguration: TextFieldConfiguration(
                  controller: nameController,
                  decoration: InputDecoration(
                      isDense: true,
                      hintText: this.widget.nameLabel,
                      hintStyle: TextStyle(fontSize: 18),
                      errorText: nameIsValid ? null : 'Нужно заполнить'
                  ),
                ),
                suggestionsCallback: (pattern) async {
                  if (pattern.isNotEmpty) {
                    return await DBProvider.db.searchProducts(pattern);
                  }
                  return [];
                },
                hideOnEmpty: true,
                hideOnLoading: true,
                itemBuilder: (context, suggestion) {
                  return ListTile(
                    title: Text(suggestion),
                  );
                },

                onSuggestionSelected: (suggestion) async {
                  Product product = await DBProvider.db.getProductByName(
                      suggestion
                  );
                  if (product != null) {
                    changeColor(product.colorForUI);

                  }
                  nameController.text = suggestion;

                },
              ),
              SizedBox(height: 10),
              TextField(
                controller: quantityController,
                decoration: InputDecoration(
                    isDense: true,
                    hintText: 'Количество',
                    hintStyle: TextStyle(fontSize: 18)
                ),
              ),
              SizedBox(height: 10),
              BlockPicker(
                  pickerColor: currentColor,
                  onColorChanged: changeColor
              ),
            ],
          ),
        )

      ),
      actions: <Widget>[
        TextButton(
          child: Text('Отменить', style: TextStyle(fontSize: 18, color: Colors.red)),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text(
            this.widget.successLabelButton,
            style: TextStyle(fontSize: 18),
          ),
          onPressed: () {
            Map<String, dynamic> data = {};
            if (nameController.text.isNotEmpty) {
              data['name'] = nameController.text;
              setState(() {
                nameIsValid = true;
              });
            } else {
              setState(() {
                nameIsValid = false;
              });
            }
            if (quantityController.text.isNotEmpty) {
              data['quantity'] = quantityController.text;
            } else {
              data['quantity'] = null;
            }
            data['color'] = currentColor.value;
            if (nameIsValid) {
              Navigator.of(context).pop(data);
            }
          },
        ),
      ],
      actionsPadding: EdgeInsets.symmetric(horizontal: 0.0),
      contentPadding: EdgeInsets.fromLTRB(25, 20, 25, 0),
    );
  }

  @override
  void dispose() {
    nameController.dispose();
    quantityController.dispose();
    super.dispose();
  }

}

