part of 'product_list_bloc.dart';

abstract class ProductListEvent extends Equatable {
  const ProductListEvent();

  @override
  List<Object> get props => [];
}


class ProductListLoad extends ProductListEvent {}


class ProductUpdate extends ProductListEvent {
  final int id;
  final String name;
  final String quantity;
  final int color;

  ProductUpdate(this.id, this.name, this.color, this.quantity);

  @override
  List<Object> get props => [id, name, color, quantity];

}


class ProductDelete extends ProductListEvent {
  final int id;

  ProductDelete(this.id);

  @override
  List<Object> get props => [id];

}


class ProductCheck extends ProductListEvent {
  final int id;

  ProductCheck(this.id);

  @override
  List<Object> get props => [id];
}


class ProductCreate extends ProductListEvent {
  final String name;
  final String quantity;
  final int color;

  ProductCreate(this.name, this.color, this.quantity);

  @override
  List<Object> get props => [name, color, quantity];

}


class CheckAllProducts extends ProductListEvent {}


class UnCheckAllProducts extends ProductListEvent {}


class DeleteAllCheckedProducts extends ProductListEvent {}


class ClearShoppingList extends ProductListEvent {}

