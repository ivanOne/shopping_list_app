
import 'package:equatable/equatable.dart';
import 'package:shopping_app/data/sqlite_models.dart';


class ProductListViewModel extends Equatable {
  final List<ShoppingListProduct> productLists;

  const ProductListViewModel(this.productLists);

  @override
  List<Object> get props => [productLists];

}