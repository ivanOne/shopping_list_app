import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shopping_app/product_list/bloc/view_model.dart';
import 'package:sqflite/sqflite.dart';

import 'package:shopping_app/data/sqlite_models.dart';
import 'package:shopping_app/data/sqlite_provider.dart';

part 'product_list_event.dart';
part 'product_list_state.dart';


class ProductListBloc extends Bloc<ProductListEvent, ProductListState> {
  final int shoppingListId;

  ProductListBloc(this.shoppingListId) : super(ProductListInit());

  @override
  Stream<ProductListState> mapEventToState(ProductListEvent event) async* {
    try {
      if (event is ProductUpdate) {
        await DBProvider.db.updateShoppingListProduct(
          event.id,
          event.name,
          event.quantity,
          event.color
        );
      }

      if (event is ProductDelete) {
        await DBProvider.db.deleteShoppingListProduct(event.id);
      }

      if (event is ProductCreate) {
        await DBProvider.db.createShoppingListProduct(
          shoppingListId,
          event.name,
          event.quantity,
          event.color
        );
      }
      
      if (event is ProductCheck) {
        await DBProvider.db.checkShoppingListProduct(event.id);
      }

      if (event is CheckAllProducts) {
        await DBProvider.db.setAllProductInShoppingList(
            shoppingListId,
            check: true
        );
      }

      if (event is UnCheckAllProducts) {
        await DBProvider.db.setAllProductInShoppingList(
            shoppingListId,
            check: false
        );
      }

      if (event is DeleteAllCheckedProducts) {
        await DBProvider.db.clearShoppingList(
            shoppingListId,
            select: 'checked'
        );
      }

      if (event is ClearShoppingList) {
        await DBProvider.db.clearShoppingList(shoppingListId);
      }
    } catch(e) {
      print(e);
      yield ProductListError();
      return;
    }
    yield* _loadProducts();
  }

  Stream<ProductListState> _loadProducts() async* {
    try {
      List<ShoppingListProduct> productList = await DBProvider.db
          .shoppingListProducts(shoppingListId);
      yield ProductListLoaded(ProductListViewModel(productList));
    } on DatabaseException {
      yield ProductListError();
    }
  }
}
