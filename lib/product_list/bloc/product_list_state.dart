part of 'product_list_bloc.dart';


abstract class ProductListState extends Equatable {
  const ProductListState();

  @override
  List<Object> get props => [];
}

class ProductListInit extends ProductListState {}

class ProductListError extends ProductListState {}

class ProductListLoaded extends ProductListState{
  final ProductListViewModel viewModel;

  ProductListLoaded(this.viewModel);


  @override
  List<Object> get props => [viewModel];
}
