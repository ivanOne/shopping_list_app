import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_app/data/sqlite_models.dart';
import 'package:shopping_app/product_list/bloc/product_list_bloc.dart';
import 'package:shopping_app/product_list/widgets/product_list_item.dart';
import 'package:shopping_app/product_list/widgets/shopping_list_action_buttons.dart';


class ProductListPage extends StatelessWidget {
  final ShoppingList shoppingList;

  const ProductListPage({Key key, this.shoppingList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ProductListBloc(shoppingList.id)..add(
          ProductListLoad()
      ),
      child: ProductListView(
        colorShoppingList: shoppingList.colorForUI,
        shoppingListName: shoppingList.name,
        shoppingListKey: shoppingList.id,
      ),
    );
  }
}


class ProductListView extends StatelessWidget {
  // TODO Переделать экшены на enum
  final Color colorShoppingList;
  final String shoppingListName;
  final int shoppingListKey;

  const ProductListView({Key key, this.colorShoppingList,
    this.shoppingListName, this.shoppingListKey}) : super(key: key);

  Future<bool> showActionConfirmDialog(
      BuildContext context,
      String action
      ) async {

    String title;
    String text;
    String actionButtonText;
    String cancelButtonText = 'Отмена';

    switch (action) {
      case 'slDelete':
        title = 'Удалить список';
        text = 'Все продукты будут удалены вместе со списком';
        actionButtonText = 'Удалить';
        break;
      case 'clear':
        title = 'Очистить список';
        text = 'Все продукты будут удалены';
        actionButtonText = 'Очистить';
        break;
      case 'deleteAllCheckedProducts':
        title = 'Удалить все помеченные';
        text = 'Удалить из списка все отмеченные продукты';
        actionButtonText = 'Удалить';
        break;
    }
    bool result = await showDialog<bool>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(text),
            actions: <Widget>[
              TextButton(
                child: Text(
                    cancelButtonText,
                    style: TextStyle(color: Colors.red, fontSize: 18)
                ),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              TextButton(
                child: Text(actionButtonText, style: TextStyle(fontSize: 18)),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        }
    );
    return result;
  }

  void runAction(String action, ProductListBloc bloc) {
    switch (action) {
      case 'slDelete':
        break;
      case 'clear':
        bloc.add(ClearShoppingList());
        break;
      case 'checkAllProducts':
        bloc.add(CheckAllProducts());
        break;
      case 'uncheckAllProducts':
        bloc.add(UnCheckAllProducts());
        break;
      case 'deleteAllCheckedProducts':
        bloc.add(DeleteAllCheckedProducts());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(shoppingListName),
        actions: [
            PopupMenuButton<String>(
              icon: Icon(
                  Icons.more_vert,
                  color: Colors.white
              ),
              onSelected: (String action) async {
                List<String> modalActions = [
                  'slDelete',
                  'clear',
                  'deleteAllCheckedProducts'
                ];
                if (modalActions.contains(action)) {

                  bool result = await showActionConfirmDialog(context, action);
                  if (result == null || !result) {
                    return;
                  }
                }

                if (action == 'slDelete') {
                  Navigator.of(context).pop(action);
                }

                runAction(action, context.read<ProductListBloc>());
              },
              itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                PopupMenuItem<String>(
                  value: 'slDelete',
                  child: Text('Удалить список'),
                ),
                PopupMenuItem<String>(
                  value: 'clear',
                  child: Text('Удалить все продукты'),
                ),
                PopupMenuItem<String>(
                  value: 'checkAllProducts',
                  child: Text('Отметить все продукты'),
                ),
                PopupMenuItem<String>(
                  value: 'uncheckAllProducts',
                  child: Text('Снять отметку со всех продуктов'),
                ),
                PopupMenuItem<String>(
                  value: 'deleteAllCheckedProducts',
                  child: Text('Удалить все купленные продукты'),
                ),
            ],
          )
        ],
        backgroundColor: colorShoppingList,
      ),
      body: Container(
        child: BlocBuilder<ProductListBloc, ProductListState>(
          builder: (context, state) {
            if (state is ProductListInit) {
              return Center(
                  child: Text(
                      'Загрузка...',
                      style: TextStyle(fontSize: 20))
              );
            } else if (state is ProductListLoaded) {
              if (state.viewModel.productLists.isEmpty) {
                return Center(
                    child: Text(
                        'Список пуст',
                        style: TextStyle(fontSize: 20))
                );
              } else {
                return ListView.builder(
                  itemCount: state.viewModel.productLists.length,
                  padding: EdgeInsets.all(8),
                  itemBuilder: (context, index) {
                    return ProductListItem(
                        slProduct: state.viewModel.productLists[index]
                    );
                  },
                );
              }
            } else {
              return Center(
                  child: Text(
                      'Произошла ошибка',
                      style: TextStyle(fontSize: 20))
              );
            }
          },
        ),
      ),
      floatingActionButton: ShoppingListAddActions(
        slKey: shoppingListKey,
        colorShoppingList: colorShoppingList,
      ),
    );
  }
}
