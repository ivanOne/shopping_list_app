part of 'shopping_list_bloc.dart';


abstract class ShoppingListState extends Equatable {
  const ShoppingListState();

  @override
  List<Object> get props => [];
}

class ShoppingListInit extends ShoppingListState {}

class ShoppingListError extends ShoppingListState {}

class ShoppingListLoaded extends ShoppingListState {
  final ShoppingListViewModel viewModel;

  ShoppingListLoaded(this.viewModel);


  @override
  List<Object> get props => [viewModel];
}
