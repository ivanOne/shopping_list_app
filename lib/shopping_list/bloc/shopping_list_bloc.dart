import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sqflite/sqflite.dart';

import 'package:shopping_app/data/sqlite_models.dart';
import 'package:shopping_app/data/sqlite_provider.dart';
import 'package:shopping_app/shopping_list/bloc/view_model.dart';

part 'shopping_list_event.dart';
part 'shopping_list_state.dart';


class ShoppingListBloc extends Bloc<ShoppingListEvent, ShoppingListState> {
  ShoppingListBloc() : super(ShoppingListInit());

  @override
  Stream<ShoppingListState> mapEventToState(ShoppingListEvent event) async* {
    try {
      if (event is ShoppingListDelete) {
        await DBProvider.db.deleteShoppingList(event.id);
      } else if (event is ShoppingListCreate) {
        await DBProvider.db.createShoppingList(
            ShoppingList(null, event.name, event.color)
        );
      } else if (event is ShoppingListUpdate) {
        await DBProvider.db.updateShoppingList(
            ShoppingList(event.id, event.name, event.color)
        );
      } else if (event is ShoppingListDeleteAll) {
        await DBProvider.db.deleteAllShoppingLists();
      }

      List<ShoppingList> slList = await DBProvider.db.allShoppingList();
      yield ShoppingListLoaded(ShoppingListViewModel(slList));
    } on DatabaseException {
      yield ShoppingListError();
      return;
    }
  }
}
