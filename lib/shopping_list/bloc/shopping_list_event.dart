part of 'shopping_list_bloc.dart';

abstract class ShoppingListEvent extends Equatable {
  const ShoppingListEvent();

  @override
  List<Object> get props => [];
}


class ShoppingListLoad extends ShoppingListEvent {}


class ShoppingListUpdate extends ShoppingListEvent {
  final int id;
  final String name;
  final int color;

  ShoppingListUpdate(this.id, this.name, this.color);

  @override
  List<Object> get props => [id, name, color];

}

class ShoppingListDelete extends ShoppingListEvent {
  final int id;

  ShoppingListDelete(this.id);

  @override
  List<Object> get props => [id];

}

class ShoppingListDeleteAll extends ShoppingListEvent {}

class ShoppingListCreate extends ShoppingListEvent {
  final String name;
  final int color;

  ShoppingListCreate(this.name, this.color);

  @override
  List<Object> get props => [name, color];

}
