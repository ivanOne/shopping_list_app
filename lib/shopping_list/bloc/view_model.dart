
import 'package:equatable/equatable.dart';
import 'package:shopping_app/data/sqlite_models.dart';


class ShoppingListViewModel extends Equatable {
  final List<ShoppingList> shoppingLists;

  const ShoppingListViewModel(this.shoppingLists);

  @override
  List<Object> get props => [shoppingLists];

}