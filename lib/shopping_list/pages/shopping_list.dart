import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_app/shopping_list/bloc/shopping_list_bloc.dart';

import 'package:shopping_app/shopping_list/widgets/form_modal.dart';
import 'package:shopping_app/shopping_list/widgets/shopping_list_item.dart';


class ShoppingListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ShoppingListBloc()..add(ShoppingListLoad()),
      child: ShoppingListView(),
    );
  }
}

class ShoppingListView extends StatelessWidget {

  List<Widget> getActions(BuildContext context) {
    ShoppingListState currentState = context.watch<ShoppingListBloc>().state;

    if (currentState is ShoppingListLoaded &&
        currentState.viewModel.shoppingLists.length > 0) {
      return  [
        RemoveIconButton(),
      ];
    }
    return [];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Списки покупок'),
        actions: getActions(context)
      ),
      body: Container(
        child: BlocBuilder<ShoppingListBloc, ShoppingListState>(
          builder: (context, state) {
            if (state is ShoppingListLoaded) {
              if (state.viewModel.shoppingLists.isEmpty) {
                return Center(
                    child: Text(
                        'У вас еще нет списков',
                        style: TextStyle(fontSize: 20))
                );
              } else {
                return ListView.builder(
                  itemCount: state.viewModel.shoppingLists.length,
                  padding: const EdgeInsets.all(8),
                  itemBuilder: (context, index) {
                    return ShoppingListCardList(
                      shoppingList: state.viewModel.shoppingLists[index],
                    );
                  },
                );
              }

            } else if (state is ShoppingListInit) {
              return Center(
                  child: Text(
                      'Загрузка...',
                      style: TextStyle(fontSize: 20))
              );
            } else {
              return Center(
                  child: Text(
                      'Произошла ошибка',
                      style: TextStyle(fontSize: 20))
              );
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          var data = await showShoppingListModalForm(
            context: context,
            successLabelButton: 'Создать',
            nameLabel: 'Название списка',
            title: 'Новый список'
          );
          if (data != null && data.isNotEmpty) {
            context.read<ShoppingListBloc>().add(
                ShoppingListCreate(data['name'], data['color'].value)
            );

          }
        }
      )
    );
  }
}

class RemoveIconButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.delete),
      tooltip: 'Удалить все списки',
      onPressed: () async {
        var result = await showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Удалить все списки?'),
                content: Text('Всё содержание списков будет удалено'),
                actions: <Widget>[
                  TextButton(
                    child: Text('Отмена', style: TextStyle(fontSize: 18),),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                  ),
                  TextButton(
                    child: Text(
                      'Удалить',
                      style: TextStyle(color: Colors.red, fontSize: 18),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                  ),
                ],
              );
            }
        );
        if (result != null && result) {
          context.read<ShoppingListBloc>().add(ShoppingListDeleteAll());

        }
      },
    );
  }
}
