import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shopping_app/data/sqlite_models.dart';
import 'package:shopping_app/product_list/pages/product_list.dart';
import 'package:shopping_app/shopping_list/bloc/shopping_list_bloc.dart';

import '../../utils.dart';
import 'form_modal.dart';


class ShoppingListCardList extends StatelessWidget {
  final ShoppingList shoppingList;

  const ShoppingListCardList({Key key, this.shoppingList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO Поработать над углами
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      color: shoppingList.colorForUI,
      child: ListTile(
          title: Text(
            shoppingList.name,
            style: TextStyle(color: Colors.white),
          ),
          onLongPress: () async {
            var dropDownResult = await showDropDownMenu(
                context,
                [
                  PopupMenuItem<int>(
                    value: 0,
                    child: Text('Удалить'),
                  ),
                  PopupMenuItem<int>(
                    value: 1,
                    child: Text('Редактировать'),
                  ),
                ]
            );
            if (dropDownResult != null) {
              switch (dropDownResult) {
                case 1: {
                  var data = await showShoppingListModalForm(
                      context: context,
                      successLabelButton: 'Сохранить',
                      nameLabel: 'Название списка',
                      title: 'Изменение списка',
                      initialColor: shoppingList.colorForUI,
                      initialName: shoppingList.name
                  );
                  if (data != null && data.isNotEmpty) {
                    context.read<ShoppingListBloc>().add(
                        ShoppingListUpdate(
                            shoppingList.id,
                            data['name'],
                            data['color'].value,
                        )
                    );
                  }
                  break;
                }
                case 0: {
                  var result = await showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('Удалить список?'),
                          content: Text('Всё содержание списка будет удалено'),
                          actions: <Widget>[
                            TextButton(
                              child: Text('Отмена',
                                  style: TextStyle(fontSize: 18)),
                              onPressed: () {
                                Navigator.of(context).pop(false);
                              },
                            ),
                            TextButton(
                              child: Text(
                                'Удалить',
                                style: TextStyle(color: Colors.red, fontSize: 18),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop(true);
                              },
                            ),
                          ],
                        );
                      });

                  if (result != null && result) {
                    context.read<ShoppingListBloc>().add(
                        ShoppingListDelete(shoppingList.id)
                    );
                  }

                }
              }
            }
          },
          onTap: () async {

            var result = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProductListPage(
                      shoppingList: shoppingList,
                    )
                )
            );

            if (result != null) {
              if (result == 'slDelete'){
                context.read<ShoppingListBloc>().add(
                    ShoppingListDelete(shoppingList.id)
                );
              }
            } else {
              context.read<ShoppingListBloc>().add(ShoppingListLoad());
            }

          },
      ),
    );
  }

}