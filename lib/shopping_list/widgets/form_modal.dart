import 'package:flutter/material.dart';
import 'package:shopping_app/widgets/block_color_picker.dart';

import '../../utils.dart';


Future<Map<String, dynamic>> showShoppingListModalForm({
  @required BuildContext context,
  @required String title,
  @required String nameLabel,
  @required String successLabelButton,
  String initialName,
  Color initialColor,
}){
  return showDialog<Map<String, dynamic>>(
    context: context,
    builder: (BuildContext context) {
      Color currentColor;
      if (initialColor != null) {
        currentColor = initialColor;
      } else {
        currentColor = availableColors[0];
      }

      TextEditingController nameController = TextEditingController();
      if (initialName != null) {
        nameController.text = initialName;
      }

      return AlertDialog(
        title: Text(title),
        content: Container(
          width: 200,
          height: 180,
          child: SingleChildScrollView(
            child: Column(
            children: <Widget>[
                TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                      isDense: true,
                      hintText: nameLabel,
                      hintStyle: TextStyle(fontSize: 18)
                  ),
                ),
                SizedBox(height: 10),
                BlockPicker(
                    pickerColor: currentColor,
                    onColorChanged: (Color color) => currentColor = color
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Отменить', style: TextStyle(fontSize: 18, color: Colors.red)),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text(successLabelButton, style: TextStyle(fontSize: 18),),
            onPressed: () {
              Map<String, dynamic> data = {};
              if (nameController.text.isNotEmpty) {
                data['name'] = nameController.text;
                data['color'] = currentColor;
              }
              Navigator.of(context).pop(data);
            },
          ),
        ],
        actionsPadding: EdgeInsets.symmetric(horizontal: 0.0),
        contentPadding: EdgeInsets.fromLTRB(25, 20, 25, 0),
      );
    },
  );
}

