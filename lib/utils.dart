import 'dart:math';

import 'package:flutter/material.dart';

const List<Color> availableColors = [
  Colors.lightBlue,
  Colors.red,
  Colors.deepPurple,
  Colors.lightGreen,
  Colors.amber,
  Colors.deepOrange,
  Colors.brown,
  Colors.blueGrey,
];

bool useWhiteForeground(Color color, {double bias: 1.0}) {
  bias ??= 1.0;
  int v = sqrt(pow(color.red, 2) * 0.299 +
      pow(color.green, 2) * 0.587 +
      pow(color.blue, 2) * 0.114)
      .round();
  return v < 130 * bias ? true : false;
}

Future<dynamic> showDropDownMenu(
    BuildContext context,
    List<PopupMenuEntry> menuItems
    ) async {
  final RenderBox item = context.findRenderObject() as RenderBox;
  final RenderBox overlay = Navigator.of(context).overlay
      .context.findRenderObject() as RenderBox;
  final RelativeRect position = RelativeRect.fromRect(
    Rect.fromPoints(
      item.localToGlobal(Offset.zero),
      item.localToGlobal(
          item.size.bottomLeft(Offset.zero),
          ancestor: overlay
      ),
    ),
    Offset.zero & overlay.size,
  );
  var action = await showMenu(
      context: context,
      position: position,
      items: menuItems
  );
  return action;
}